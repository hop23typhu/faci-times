<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'faci-times');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9uQ}@KByJ;.iWb5,S74ttp}][sVfV8Z3<6+;;G3-o+7cjHmEB{NEOM4#NwC}:bDH');
define('SECURE_AUTH_KEY',  '|b7e>>PGl9+LGpBe|T1!DsiPC`;ap{5OJwWI7T-+%}X7z1$)$,XAnrft(.w}H5!@');
define('LOGGED_IN_KEY',    ',1,R3Lj)p*SEY<nEmlj7(a@+y*t;s|%PCH3Z_l~Zt{!7PpV.)]89ihKtn^/%2,V=');
define('NONCE_KEY',        'G!@0RBgp<y0+A+yNI{~ym=f]2Md/uU!bGB]<9$_p oB#O+uqRg!IxXb1YSsp//f+');
define('AUTH_SALT',        'fnJq.-1yvSFS3-^ephE1>[ y%s]dAWrTJ b{Npz-<z2$sw6ftBbi`]1-Ee8QYA.L');
define('SECURE_AUTH_SALT', '70P-3g1I3-^?AvD5A-i/S2u4f#1&auMA{ VBiyZ|Vp#i>|%v?M`7{Q2+G2tOh[)X');
define('LOGGED_IN_SALT',   '>QcRVu/Q.cprFLoZ%@gH*[6*0L{FT)w($:$(5niLA.QWG2>GBc(rd4Q-x;9BH_|#');
define('NONCE_SALT',       '}`b>Sxc|VmKc=-$,n+K*-#QO|}Dd&_|3}@u)`+(7Df@+)#vOIm.bge]RH{EjE0/>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'faci_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
