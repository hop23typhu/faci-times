<?php  
$faci_lang=array(
	'Xem tiếp...'=>'Read more...',
	'Hôm nay'=>'Today',
	'Tổng lượt xem'=>'Total views',
	'Thống kê truy cập'=>'Statistical access',
	'Tìm kiếm'=>'Search',
	'Bài viết liên quan'=>'Related posts',
	'Từ khóa tìm kiếm'=>'Keyword',
	'Chúng tôi không thể tìm thấy những gì bạn đang tìm kiếm. Có lẽ tìm kiếm có thể giúp đỡ bạn .'=>'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.',
	'Xin lỗi, nhưng không có gì phù hợp với điều kiện tìm kiếm của bạn. Vui lòng thử lại với một số từ khóa khác'=>'Sorry, but nothing matched your search terms. Please try again with some different keywords',
	
);