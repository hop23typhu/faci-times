<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <base href="<?php bloginfo('wpurl'); ?>/">
    <meta property="fb:admins" content="9f72af4a100aeceb56beb5cff0b76384"/>
    <meta property="fb:app_id" content="1171840959527199" /> 
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300&subset=latin,vietnamese"  rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css"  rel="stylesheet" type='text/css'>
    <link href="<?php echo get_template_directory_uri(); ?>/style.css"  rel="stylesheet" type='text/css'>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>

    <?php wp_head(); ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(); ?>>
<div id="wrapper">
    <header id="header">
        <div class="container">
            <div id="logo">
                <a href="<?php bloginfo( 'wpurl' ); ?>"  title="<?php bloginfo( 'name' ); ?>">
                  <img src="<?php echo print_option( 'site-logo' ); ?>"  alt="<?php bloginfo( 'name' ); ?>"/>
                </a>
            </div>
            <div class="banner-header">
              <?php if( get_theme_mod( 'img_banner' ) ) echo '<img src="'.get_theme_mod( "img_banner" ).'" alt="banner"/>'; ?>
            </div>
        </div>
    </header>
    <nav id="menu-main" class="navbar navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse" style="height: 1px;">
              <?php 
                   /**
                  * Displays a navigation menu
                  * @param array $args Arguments
                  */
                  $args = array(
                    'theme_location' => 'primary-menu',
                    'container' => '',
                    'items_wrap' => '<ul id = "menu-top1" class = "%2$s menu nav navbar-nav">%3$s</ul>',
                  );
                  wp_nav_menu( $args );
              ?>
            </div>

        </div>

    </nav>
  <?php 
    if( is_home() || is_front_page() ){
       echo '<div id="slider">';
         echo do_shortcode( '[rev_slider main]' );
       echo '</div>';
    }
  ?>
<main id="main-content">
<?php  
if( !is_home() && !is_front_page() ){
  echo '<div class="breadcrumb container">';
  bcn_display(); 
  echo '</div>';
}
?>