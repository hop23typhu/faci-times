<?php 
/*
 * template name: toàn màn hình
*/
?>
<?php get_header(); ?>
<div class="content-wrapper nd-khoi">
  <div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <?php if(have_posts()):while(have_posts()):the_post(); ?>
          <h1 class="title-page"><?php the_title(); ?></h1>
          <?php the_content(); ?>
          <?php endwhile; else : get_template_part('template-parts/content','none'); endif; ?> 
        </div>
      <!-- end #content -->
    </div>
  </div>
</div> <!-- end .content-wrapper -->
<?php get_footer(); ?>