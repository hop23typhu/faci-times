<?php 
/*
 * template name: faq
*/
?>
<?php get_header(); ?>
<div class="content-wrapper nd-khoi">
  <div class="container">
    <div class="row">
      <div id="content" class="col-xs-12 col-sm-12 col-md-9 col-lg-9 pull-right">
              <div class="post-entry">
                  <?php if(have_posts()):while(have_posts()):the_post(); ?>
                  <h1 class="title-page"><?php the_title(); ?></h1>
                     <div class="container">   
                        <div class="row">
                            <div class="col-md-9 ">
                                <!-- Nav tabs category -->
                                 <?php 
                                    $taxonomy = 'faq-category';
                                    $args = array(
                                        'orderby'           => 'menu_order', 
                                        'order'             => 'asc',
                                        'hide_empty'        => false,
                                        'fields'            => 'all', 
                                    ); 
                                    $tax_terms = get_terms($taxonomy,$args);
                                ?>
                                <ul class="nav nav-tabs faq-cat-tabs">
                                  <?php $stt=0; foreach ( $tax_terms as $tax_term ) { $stt++; ?>
                                    <li class="<?php if($stt==1) echo 'active'; ?>"><a href="#faq-cat-<?php echo $stt; ?>" data-toggle="tab"><?php echo esc_html( $tax_term->name ); ?></a></li>
                                   <?php } ?>
                                </ul>
                        
                                <!-- Tab panes -->
                                <div class="tab-content faq-cat-content">

                                    <?php $stt=0; foreach ( $tax_terms as $tax_term ) { $stt++; ?>
                                    <div class="tab-pane <?php if($stt==1) echo 'active'; ?> in fade" id="faq-cat-<?php echo $stt; ?>">
                                        <div class="panel-group" id="accordion-cat-1">

                                          <?php  
                                            $query_item_in_cat=null;
                                            $query_item_in_cat= new WP_Query(
                                              array(
                                                'tax_query' => array(
                                                  array(
                                                    'taxonomy' => 'faq-category',
                                                    'terms'    => array($tax_term->term_id),
                                                  ),
                                                ),
                                                'post_type'=>array( 'faq'),
                                                'order' => 'ASC' 
                                              )
                                            );
                                            $i=0;
                                            while($query_item_in_cat->have_posts()):$query_item_in_cat->the_post();$i++;
                                          ?>
                                            <div class="panel panel-default panel-faq">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#accordion-cat-<?php echo $stt; ?>" href="#faq-cat-<?php echo $stt; ?>-sub-<?php echo $i; ?>">
                                                        <h4 class="panel-title">
                                                            <?php the_title(); ?>
                                                            <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                                        </h4>
                                                    </a>
                                                </div>
                                                <div id="faq-cat-<?php echo $stt; ?>-sub-<?php echo $i; ?>" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <?php the_content(); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endwhile; wp_reset_postdata(); ?>
                                        </div>
                                    </div>
                                    <?php } ?>



                                </div>
                              </div>
                            </div>
                        </div>
                  <?php endwhile; else : get_template_part('template-parts/content','none'); endif; ?> 
                  
              </div> <!-- end .post-entry -->
          </div>
      <!-- end #content -->
    <?php get_sidebar(); ?>     
    </div>
  </div>
</div> <!-- end .content-wrapper -->

<?php get_footer(); ?>