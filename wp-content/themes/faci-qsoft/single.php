<?php get_header(); ?>
<div class="content-wrapper nd-khoi">
  <div class="container">
    <div class="row">
      <div id="content" class="col-xs-12 col-sm-12 col-md-9 col-lg-9 pull-right">
              <div class="post-entry">
                  <?php if(have_posts()):while(have_posts()):the_post(); ?>
                  <h1 class="title-page"><?php the_title(); ?></h1>
                   <div class="post-content">
                      <?php the_content(); ?>
                      <footer>
                        <section class="kun-social">
                          <div class="button"><div class="fb-like" data-href="<?php the_permalink();?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div></div>
                          <div class="button"><div class="fb-share-button" data-href="<?php the_permalink();?>" data-type="button_count"></div></div>
                          <div class="button"><div class="g-plus" data-action="share" data-annotation="bubble"></div></div>
                          <div class="button"><div class="g-plusone" data-size="medium"></div></div>
                          <div class="clear"></div>
                        </section><!-- .kun-social -->
                        
                        <section class="comment-social">
                          <div style="width:100%" class="fb-comments" data-href="<?php the_permalink();?>" data-width="700" data-numposts="5" data-colorscheme="light"></div>
                        </section><!-- .comment-social -->
                      </footer>
                   </div>
                  <?php endwhile; else : get_template_part('template-parts/content','none'); endif; ?> 
                  <?php  
                      $id = get_the_id();
                      $terms = get_the_terms( $id, 'category' );
                      $arrid=array();
                      foreach($terms as $term) {
                          $arrid=$term->term_id;   
                      }
                    $args_q = array(
                        'orderby'=>'ID',
                        'order'=>'DESC',
                        'post__not_in'=>array($id),
                        'tax_query' => array(
                          array(
                            'taxonomy' => 'category',
                            'terms' => $arrid
                          )
                        )
                    );
                    $rela_post=new WP_Query($args_q); 
                    if($rela_post->have_posts()){ 
                  ?>
                  <div class="post-related">
                      <h3 class="title">Bài viết liên quan</h3>
                      <div class="row">
                        <?php while($rela_post->have_posts()){ $rela_post->the_post(); ?>
                          <div class="post-thumb col-xs-12 col-sm-6 col-md-6">
                              <a href="<?php the_permalink(); ?>">
                                <?php 
                                    if(has_post_thumbnail( ))
                                        the_post_thumbnail('large',array('alt'=>get_the_title(),'class'=>'img-thumb img-responsive','width'=>'90px','height'=>'auto'  ));
                                    else echo ' <img src="'.get_theme_mod("img_error").'" alt="'.get_the_title().'"  class="img-thumb img-responsive" width="90px" height="auto" />';
                                ?>
                              </a>
                              <h3 class="title-thumb"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                          </div>
                          <?php } ?>
                      </div>
                  </div>
                  <?php } ?>
              </div> <!-- end .post-entry -->
              
          </div>
      <!-- end #content -->
    <?php get_sidebar(); ?>     
    </div>
  </div>
</div> <!-- end .content-wrapper -->
<?php get_footer(); ?>