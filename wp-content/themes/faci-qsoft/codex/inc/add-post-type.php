<?php
	// Create Post Type Accompishment
	function create_accompishment() {
	$labels = array(
    'name' => _x('Accompishment', 'post type general name', 'your_text_domain'),
    'singular_name' => _x('Accompishment', 'post type singular name', 'your_text_domain'),
    'add_new' => _x('Add New', 'Accompishment', 'your_text_domain'),
    'add_new_item' => __('Add New Accompishment', 'your_text_domain'),
    'edit_item' => __('Edit Accompishment', 'your_text_domain'),
    'new_item' => __('New Accompishment', 'your_text_domain'),
    'all_items' => __('All Accompishment', 'your_text_domain'),
    'view_item' => __('View Accompishment', 'your_text_domain'),
    'search_items' => __('Search Accompishment', 'your_text_domain'),
    'not_found' =>  __('No Accompishment found', 'your_text_domain'),
    'not_found_in_trash' => __('No Accompishment found in Trash', 'your_text_domain'), 
    'parent_item_colon' => '',
    'menu_name' => __('Accompishment', 'your_text_domain')
    );
	$args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array( 'slug' => _x( 'accompishment', 'URL slug', 'your_text_domain' ) ),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => 5,
	'menu_icon' =>  'dashicons-share-alt2',
    'supports' => array( 'title','editor','thumbnail')
	); 
	register_post_type('accompishment', $args);
	}
	add_action( 'init', 'create_accompishment' );
	// End Post Type Accompishment
	// Create Post Type Gallery
	function create_gallery() {
	$labels = array(
    'name' => _x('Gallery', 'post type general name', 'your_text_domain'),
    'singular_name' => _x('Gallery', 'post type singular name', 'your_text_domain'),
    'add_new' => _x('Add New', 'Gallery', 'your_text_domain'),
    'add_new_item' => __('Add New Gallery', 'your_text_domain'),
    'edit_item' => __('Edit Gallery', 'your_text_domain'),
    'new_item' => __('New Gallery', 'your_text_domain'),
    'all_items' => __('All Gallery', 'your_text_domain'),
    'view_item' => __('View Gallery', 'your_text_domain'),
    'search_items' => __('Search Gallery', 'your_text_domain'),
    'not_found' =>  __('No Gallery found', 'your_text_domain'),
    'not_found_in_trash' => __('No Gallery found in Trash', 'your_text_domain'), 
    'parent_item_colon' => '',
    'menu_name' => __('Gallery', 'your_text_domain')
    );
	$args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array( 'slug' => _x( 'gallery', 'URL slug', 'your_text_domain' ) ),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => 5,
	'menu_icon' =>  'dashicons-images-alt2',
    'supports' => array( 'title','editor','thumbnail')
	); 
	register_post_type('gallery', $args);
	}
	add_action( 'init', 'create_gallery' );
	// End Post Type Gallery
	// Create Post Type Crew
	function create_Crew() {
	$labels = array(
    'name' => _x('Crew', 'post type general name', 'your_text_domain'),
    'singular_name' => _x('Crew', 'post type singular name', 'your_text_domain'),
    'add_new' => _x('Add New', 'Crew', 'your_text_domain'),
    'add_new_item' => __('Add New Crew', 'your_text_domain'),
    'edit_item' => __('Edit Crew', 'your_text_domain'),
    'new_item' => __('New Crew', 'your_text_domain'),
    'all_items' => __('All Crew', 'your_text_domain'),
    'view_item' => __('View Crew', 'your_text_domain'),
    'search_items' => __('Search Crew', 'your_text_domain'),
    'not_found' =>  __('No Crew found', 'your_text_domain'),
    'not_found_in_trash' => __('No Crew found in Trash', 'your_text_domain'), 
    'parent_item_colon' => '',
    'menu_name' => __('Crew', 'your_text_domain')
    );
	$args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array( 'slug' => _x( 'crew', 'URL slug', 'your_text_domain' ) ),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => 5,
	'menu_icon' =>  'dashicons-welcome-learn-more',
    'supports' => array( 'title','thumbnail')
	); 
	register_post_type('crew', $args);
	}
	add_action( 'init', 'create_crew' );
	// End Post Type Crew
	// Create Post Type Race
	function create_race() {
	$labels = array(
    'name' => _x('Race', 'post type general name', 'your_text_domain'),
    'singular_name' => _x('Race', 'post type singular name', 'your_text_domain'),
    'add_new' => _x('Add New', 'Race', 'your_text_domain'),
    'add_new_item' => __('Add New Race', 'your_text_domain'),
    'edit_item' => __('Edit Race', 'your_text_domain'),
    'new_item' => __('New Race', 'your_text_domain'),
    'all_items' => __('All Race', 'your_text_domain'),
    'view_item' => __('View Race', 'your_text_domain'),
    'search_items' => __('Search Race', 'your_text_domain'),
    'not_found' =>  __('No Race found', 'your_text_domain'),
    'not_found_in_trash' => __('No Race found in Trash', 'your_text_domain'), 
    'parent_item_colon' => '',
    'menu_name' => __('Race', 'your_text_domain')
    );
	$args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array( 'slug' => _x( 'race', 'URL slug', 'your_text_domain' ) ),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => 5,
	'menu_icon' =>  'dashicons-performance',
    'supports' => array( 'title','thumbnail','excerpt')
	); 
	register_post_type('race', $args);
	}
	add_action( 'init', 'create_race' );
	// End Post Type Race
?>
