<?php
<!-------------------- Create sidebar widget admin ------------------------>

  register_sidebar( array (
    'name' => __( 'Sidebar Banner', 'blankslate' ),
    'id' => 'primary-banner',
    'before_widget' => '<div class="widget-container-banner %2$s">',
    'after_widget' => "</div>",
    'before_title' => '<h3 class="widget-title-banner">',
    'after_title' => '</h3>',
    ) );
  register_sidebar( array (
    'name' => __( 'Top Banner', 'blankslate' ),
    'id' => 'primary-top-banner',
    'before_widget' => '<div class="widget-container-top-banner %2$s">',
    'after_widget' => "</div>",
    'before_title' => '<h3 class="widget-title-banner">',
    'after_title' => '</h3>',
    ) );

  //php dynamic_sidebar('primary-top-banner'); 
  //php dynamic_sidebar('Sidebar Banner'); 
<!-------------------- Create widget admin title ------------------------>

class Dunghoang_Widget extends WP_Widget {
  
  /**
   * Thiết lập widget: đặt tên, base ID
   */
  function Dunghoang_Widget() {
    $tpwidget_options = array(
      'classname' => 'dunghoang_widget_class', //ID của widget
      'description' => 'Mô tả của widget'
    );
    $this->WP_Widget('dunghoang_widget_id', 'DungHoang Widget', $tpwidget_options);
  }
  
  /**
   * Tạo form option cho widget
   */
  function form( $instance ) {
    
    //Biến tạo các giá trị mặc định trong form
    $default = array(
      'title' => 'Tiêu đề widget'
    );
    
    //Gộp các giá trị trong mảng $default vào biến $instance để nó trở thành các giá trị mặc định
    $instance = wp_parse_args( (array) $instance, $default);
    
    //Tạo biến riêng cho giá trị mặc định trong mảng $default
    $title = esc_attr( $instance['title'] );
    
    //Hiển thị form trong option của widget
    echo "<p>Nhập tiêu đề <input type='text' class='widefat' name='".$this->get_field_name('title')."' value='".$title."' /></p>";
    
    
  }
  
  /**
   * save widget form
   */
  
  function update( $new_instance, $old_instance ) {
    
    $instance = $old_instance;
    $instance['title'] = strip_tags($new_instance['title']);
    return $instance;
  }
  
  /**
   * Show widget
   */
  
  function widget( $args, $instance ) {
    
    extract( $args );
    $title = apply_filters( 'widget_title', $instance['title'] );
    
    echo $before_widget;
    
    //In tiêu đề widget
    echo $before_title.$title.$after_title;
    
    // Kết thúc nội dung trong widget
    
    echo $after_widget;
  }
  
}

/*
 * Khởi tạo widget item
 */
add_action( 'widgets_init', 'create_dunghoang_widget' );
function create_dunghoang_widget() {
  register_widget('Dunghoang_Widget');
}


<!---------------------Random post ------------------------>

class Random_Post extends WP_Widget {
 
    function __construct() {
        parent::__construct(
            'random_post',
            'Bài ngẫu nhiên',
            array( 'description'  =>  'Widget hiển thị bài viết ngẫu nhiên' )
        );
    }
 
    function form( $instance ) {
 
        $default = array(
            'title' => 'Tiêu đề widget',
            'post_number' => 10
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $post_number = esc_attr($instance['post_number']);
 
        echo '<p>Nhập tiêu đề <input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo '<p>Số lượng bài viết hiển thị <input type="number" class="widefat" name="'.$this->get_field_name('post_number').'" value="'.$post_number.'" placeholder="'.$post_number.'" max="30" /></p>';
 
    }
 
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['post_number'] = strip_tags($new_instance['post_number']);
        return $instance;
    }
 
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $post_number = $instance['post_number'];
 
        echo $before_widget;
        echo $before_title.$title.$after_title;
        $random_query = new WP_Query('posts_per_page='.$post_number.'&orderby=rand');
 
        if ($random_query->have_posts()):
            echo "<ol>";
            while( $random_query->have_posts() ) :
                $random_query->the_post(); ?>
 
                <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
 
            <?php endwhile;
            echo "</ol>";
        endif;
        echo $after_widget;
 
    }
 
}
 
add_action( 'widgets_init', 'create_randompost_widget' );
function create_randompost_widget() {
    register_widget('Random_Post');
}

<!---------------------New post ------------------------>


class thachpham_recent_posts extends WP_Widget {

  function __construct() {
    parent::__construct(
      'thachpham-recent-posts',
      'Bài viết mới',
      array( 'description' => 'Hiển thị bài viết mới', )
      );
  }

  public function widget($args, $instance) {
    extract($args);
    $title = apply_filters( 'widget_title', $instance['title'] );
    $number = $instance['number'];

    echo $before_widget . $before_title;
    if ( ! empty ( $title ) ) {
      echo $title;
    }
    echo $after_title;

    $args = array (
      'posts_per_page' => $number,
      );
    $neatly_posts = new WP_Query($args);
    if( $neatly_posts->have_posts() ) {
      echo '<ul>';
      while( $neatly_posts->have_posts() ) : $neatly_posts->the_post(); ?>
      <li>
        <div class="post-thumbnail">
          
          <a href="<?php the_permalink(); ?>"><?php echo short_title('...', 20); ?></a>

          <!-- <?php echo the_post_thumbnail(); ?> -->
        </div>

      </li>
    <?php endwhile;
  }

  wp_reset_postdata();

  echo $after_widget;
}

public function form( $instance ) {
  $title = strip_tags($instance['title']);
  if (isset( $instance['number' ] ) ) {
    $number = $instance['number'];
  } else { $number = '5'; }

  ?>
  <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
    <p><em>Use the following options to customize the display.</em></p>

    <p style="border-bottom:4px double #eee;padding: 0 0 10px;">
      <label for="<?php echo $this->get_field_id( 'number' ); ?>">Số lượng bài viết cần hiển thị</label>
      <input id="<?php echo $this->get_field_id( 'number'); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo esc_attr($number); ?>" type="number" style="width:100%;" /><br>
    </p>

    <?php }

    public function update( $new_instance, $old_instance ) {
      $instance = $old_instance;

      $instance['title'] = strip_tags($new_instance['title']);
      $instance['number'] = strip_tags($new_instance['number']);

      return $instance;
    }

  }

  function register_recent_posts() {
    register_widget( 'thachpham_recent_posts' );
  }
  add_action( 'widgets_init', 'register_recent_posts' );

<!---------------------------- create yahoo ------------------------------->

/*
 * Tạo Yahoo status widget
 * */
class Yahoo_Status extends WP_Widget {

  //Khởi tạo contructor của 1 lớp
  function Yahoo_Status(){
    parent::WP_Widget('Yahoo_Status_Widget', 
      'Yahoo Status', 
      array('description' => 'Trạng thái nick yahoo.'));
  }
    
  function widget( $args, $instance ) {
    extract($args);
    $title = apply_filters( 'widget_title', 
      empty($instance['title']) ? '' : $instance['title'], 
      $instance, $this->id_base);
    $text = apply_filters( 'widget_text', 
      $instance['text'], $instance );
    echo $before_widget;
    if ( !empty( $title ) ) { 
      echo $before_title . $title . $after_title; } ?>      
      <ul class="nick_status">
      <?php $str = explode(",", $text);       
        foreach ($str as $s) : ?>
          <li>
            <a href="ymsgr:sendIM?<?php echo trim($s);?>">
            <img border="0" src="http://presence.msg.yahoo.com/online?u=<?php echo trim($s);?>&m=g&t=1&l=us">
          </a>
          </li>
      <?php endforeach;           
      ?>
      </ul>     
    <?php
    echo $after_widget;
  }

  function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $instance['title'] = strip_tags($new_instance['title']);
    if ( current_user_can('unfiltered_html') )
      $instance['text'] =  $new_instance['text'];
    else
      $instance['text'] = stripslashes( 
        wp_filter_post_kses( addslashes($new_instance['text']) ) 
      );    
    return $instance;
  }

  function form( $instance ) {
    $instance = wp_parse_args( (array) $instance, 
      array( 'title' => '', 'text' => '' ) );
    $title = strip_tags($instance['title']);
    $text = format_to_edit($instance['text']);
?>
    <p>
      <label for="<?php echo $this->get_field_id('title'); ?>">
        <?php _e('Tiêu đề:'); ?> </label>
      <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" 
        name="<?php echo $this->get_field_name('title'); ?>" type="text" 
        value="<?php echo  esc_attr($title);?>" />
    </p>
      <label for="<?php echo $this->get_field_id('text'); ?>">
        <?php _e('Nick yahoo(cách nhau bởi dấu \'):'); ?> </label>
      <textarea class="widefat" rows="5" cols="10" 
        id="<?php echo $this->get_field_id('text'); ?>" 
        name="<?php echo $this->get_field_name('text'); ?>">
          <?php echo $text;?>
      </textarea>
    <p>
      <label for="<?php echo $this->get_field_id('text'); ?>">
        <?php _e('Ví dụ: tanvannguyen18, meocon17'); ?>
      </label>      
    </p>
<?php
  }
}
    
register_widget('Yahoo_Status');

//PLUGIN
WP Social Networks
Image Widget
Van ban - dia chi, thong tin lien lac

