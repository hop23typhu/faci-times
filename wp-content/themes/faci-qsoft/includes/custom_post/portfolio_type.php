<?php

$args = array(
    "label"                         => "Nhóm danh mục", 
    "singular_label"                => "Nhóm danh mục", 
    'public'                        => true,
    'hierarchical'                  => true,
    'show_ui'                       => true,
    'show_in_nav_menus'             => false,
    'args'                          => array( 'orderby' => 'term_order' ),
    'rewrite'                       => false,
    'query_var'                     => true
);

register_taxonomy( 'portfolio-category', 'portfolio', $args );

    
add_action('init', 'portfolio_register');  
  
function portfolio_register() {  
    global $themename;
    $labels = array(
        'name'               => __('Thư viện ảnh', 'post type general name', $themename),
        'singular_name'      => __('Thư viện ảnh', 'post type singular name', $themename),
       
    );

    $args = array(  
        'labels'            => $labels,  
        'public'            => true,  
        'show_ui'           => true,
        'show_in_menu'      => true,
        'show_in_nav_menus' => false,
        'rewrite'           => false,
        'supports'          => array('title', 'editor', 'thumbnail'),
        'has_archive'       => true,
        'taxonomies'        => array('portfolio-category'),
        'menu_icon'         => 'dashicons-format-gallery'
       );  
  
    register_post_type( 'portfolio' , $args );  
}

?>