<?php
/** Team **/
add_action( 'init', 'create_team' );
function create_team() {
    global $themename;
    $labels = array(
        'name'               => __('Team', $themename),
        'singular_name'      => __('Team', $themename),
        
    );
 
    register_post_type( 'team', array(
        'labels'            => $labels,  
        'public'            => false,  
        'show_ui'           => true,
        'show_in_menu'      => true,
        'show_in_nav_menus' => false,
        'rewrite'           => false,
        'supports'          => array('title', 'editor', 'thumbnail'),
        'menu_icon'         =>'dashicons-id-alt'
    ) );
}
?>