<?php
/**
 * deeds Theme Customizer.
 *
 * @package deeds
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function deeds_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'deeds_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 * Js 
 */
function deeds_customize_preview_js() {
	wp_enqueue_script( 'deeds_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'deeds_customize_preview_js' );

function faci_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'faci_session' , array(
	    'title'      => 'Faci customizer',
	    'priority'   => 30,
	) );
	//1 tuỳ chỉnh gồm 2 thành phần setting , và control
	/*$wp_customize->add_setting( 'color_general' , array(
	    'default'     => '#000'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'control_color_general', array(
		'label'        =>'Tông màu chính',
		'section'    => 'faci_session',
		'settings'   => 'color_general',
	) ) );
	$wp_customize->add_setting( 'color_link_tag' , array(
	    'default'     => '#337ab7'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'control_color_link_tag', array(
		'label'        =>'Màu thẻ điều hướng',
		'section'    => 'faci_session',
		'settings'   => 'color_link_tag',
	) ) );*/
	//Quản lý gửi liên hệ thành công
	$wp_customize->add_setting( 'txt_csuccess' , array(
	    'default'     => ''
	) );
	$wp_customize->add_control(
		'control_txt_csuccess', 
		array(
			'label'    => 'Thông báo gửi liên hệ thành công',
			'section'  => 'faci_session',
			'settings' => 'txt_csuccess',
			'type'     => 'textarea',
		)
	);
	//Quản lý gửi liên hệ thành công
	$wp_customize->add_setting( 'txt_cerror' , array(
	    'default'     => ''
	) );
	$wp_customize->add_control(
		'control_txt_cerror', 
		array(
			'label'    => 'Thông báo gửi liên hệ thất bại',
			'section'  => 'faci_session',
			'settings' => 'txt_cerror',
			'type'     => 'textarea',
		)
	);

	//Quản lý text close
	$wp_customize->add_setting( 'txt_close' , array(
	    'default'     => 'Close'
	) );
	$wp_customize->add_control(
		'control_txt_close', 
		array(
			'label'    => 'Đóng helper',
			'section'  => 'faci_session',
			'settings' => 'txt_close',
			'type'     => 'text',
		)
	);
	//Quản lý text phone
	$wp_customize->add_setting( 'txt_phone' , array(
	    'default'     => ''
	) );
	$wp_customize->add_control(
		'control_txt_phone', 
		array(
			'label'    => 'Số điện thoại',
			'section'  => 'faci_session',
			'settings' => 'txt_phone',
			'type'     => 'tel',
		)
	);
	//Quản lý ảnh helper
	$wp_customize->add_setting( 'img_helper' , array(
	    'default'     => '',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( 
		$wp_customize, 
		'img_helper_control', 
		array(
			'label'      => 'Ảnh helper',
			'section'    => 'faci_session',
			'settings'   => 'img_helper',
		)
	));

	//Quản lý ảnh banner
	$wp_customize->add_setting( 'img_banner' , array(
	    'default'     => '',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( 
		$wp_customize, 
		'img_banner_control', 
		array(
			'label'      => 'Ảnh banner',
			'section'    => 'faci_session',
			'settings'   => 'img_banner',
		)
	));
	//Quản lý ảnh back to top
	$wp_customize->add_setting( 'img_backtop' , array(
	    'default'     => '',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( 
		$wp_customize, 
		'img_backtop_control', 
		array(
			'label'      => 'Ảnh back to top',
			'section'    => 'faci_session',
			'settings'   => 'img_backtop',
		)
	));
	
	//Quản lý ảnh bị lỗi
	$wp_customize->add_setting( 'img_error' , array(
	    'default'     => '',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( 
		$wp_customize, 
		'imgnotfound_control', 
		array(
			'label'      => 'Ảnh lỗi 404',
			'section'    => 'faci_session',
			'settings'   => 'img_error',
		)
	));

}
add_action( 'customize_register', 'faci_customize_register' );//thực thi hàm
