<?php
function custom_stylesheet() {
	$color_general= get_theme_mod('color_general');
	$color_link_tag= get_theme_mod('color_link_tag');
?>
<style type="text/css">
	/* body{
		color: <?php echo $color_general; ?>;
	}
	a{
		color: <?php echo $color_link_tag; ?>;
	} */
	.alignnone {
		margin: 5px 20px 20px 0;
	}
	.aligncenter, div.aligncenter {
		display:block;
		margin: 5px auto 5px auto;
	}
	.alignright {
		float:right;
		margin: 5px 0 20px 20px;
	}
	.alignleft {
		float:left;
		margin: 5px 20px 20px 0;
	}
	.aligncenter {
		display: block;
		margin: 5px auto 5px auto;
	}
	a img.alignright {
		float:right;
		margin: 5px 0 20px 20px;
	}
	a img.alignnone {
		margin: 5px 20px 20px 0;
	}
	a img.alignleft {
		float:left;
		margin: 5px 20px 20px 0;
	}
	a img.aligncenter {
		display: block;
		margin-left: auto;
		margin-right: auto
	}
	.wp-caption {
		background: #fff;
		border: 1px solid #f0f0f0;
		max-width: 96%; 
		padding: 5px 3px 10px;
		text-align: center;
	}
	.wp-caption.alignnone {
		margin: 5px 20px 20px 0;
	}
	.wp-caption.alignleft {
		margin: 5px 20px 20px 0;
	}
	.wp-caption.alignright {
		margin: 5px 0 20px 20px;
	}
	.wp-caption img {
		border: 0 none;
		height: auto;
		margin:0;
		max-width: 98.5%;
		padding:0;
		width: auto;
	}
	.wp-caption p.wp-caption-text {
		font-size:11px;
		line-height:17px;
		margin:0;
		padding:0 4px 5px;
	}

</style>
<?php
}
add_action('wp_head', 'custom_stylesheet', 50);

?>

