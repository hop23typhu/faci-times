<li>
   <a href="<?php  the_permalink(); ?>" class="img-thumb">
        <?php 
            if(has_post_thumbnail( ))
                the_post_thumbnail('large',array('alt'=>get_the_title(),'class'=>' img-responsive' ,'width'=>' 64','height'=>' auto' ));
            else echo ' <img src="'.get_theme_mod("img_error").'" alt="'.get_the_title().'"  class="img-responsive" width="64" height="auto" />';
        ?>
    <h3 class="title-thumb"><a href="<?php  the_permalink(); ?>"><?php the_title(); ?></a></h3>
</li>